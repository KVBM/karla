﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;



    class FabricacionDeCasas
    {
        private static Semaphore PC;
        private static int _padding;
        private static int n = 0;
        private static int m = 0;
        private static int o = 0;
        private static int c = 0;
        private static int cont = 0;

        //Procesamientos

        private static void entrada()
        {


        }

        private static void trabajo(object trabajador)
        {
            Console.WriteLine("Trabajador del Albañil");
            Console.WriteLine("Thread {0}: {1}, Priority {2}",
                        Thread.CurrentThread.ManagedThreadId,
                        Thread.CurrentThread.ThreadState,
                        Thread.CurrentThread.Priority);
            Console.WriteLine("Trabajador {0} esperando el turno...", trabajador);
            PC.WaitOne();

            int padding = Interlocked.Add(ref _padding, 100);

            Console.WriteLine("Trabajador {0} obtiene el turno...", trabajador);
            Thread.Sleep(1000 + padding);
            Console.WriteLine("Trabajador {0} termina el turno...", trabajador);
            Console.WriteLine("Trabajador {0} libera su lugar {1}",
                trabajador, PC.Release());

        }
        private static void ConstruirCasas(int cantidad)
        {
            for (int i = 1; i <= cantidad; i++)
            {

                Console.WriteLine("Construyendo casa {0}", i);
            }
        }

        private static void ConstruirBanos(int[] cantidad)
        {
            for (int i = 1; i <= cantidad.Length; i++)
            {

                Console.WriteLine("Construyendo baño {0}", i);
            }
        }
        private static void ConstruirHab(int[] cantidad)
        {
            for (int i = 1; i <= cantidad.Length; i++)
            {
                Console.WriteLine("Construyendo habitación {0}", i);
            }
        }

        public static async Task Main(string[] args)
        {

            Console.WriteLine("\n\t\t\tF A B R I C A C I O N  D E   C A S A S ");
            Console.WriteLine("\n\n\t______________________________________________________________");
            Console.WriteLine("\n\t¿Cuántos trabajadores   llegaron?");
            n = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("\t¿Cuántos pueden trabajar?");

            m = Convert.ToInt32(Console.ReadLine());


            Console.WriteLine("\t¿ segundos para contruir?");

            o = Convert.ToInt32(Console.ReadLine()) * 1000;
            Console.WriteLine("\t¿Cuántas casas se van a construir?");
            c = Convert.ToInt32(Console.ReadLine());


            int[] ba = new int[c];
            int[] hb = new int[c];

            for (int i = 0; i < hb.Length; i++)
            {
                Console.WriteLine("\t¿Cuántas habitaciones harán en la casa {0}?", i + 1);
                hb[i] = Convert.ToInt32(Console.ReadLine());
            }
            for (int i = 0; i < ba.Length; i++)
            {
                Console.WriteLine("\t¿Cuántos baños harán en la casa {0}?", i + 1);
                ba[i] = Convert.ToInt32(Console.ReadLine());
            }


            PC = new Semaphore(0, m);

            Console.WriteLine("\tInicia la construcción");

            for (int i = 1; i <= n; i++)
            {
                Thread t = new Thread(new ParameterizedThreadStart(trabajo));
                if (i % 2 == 0)
                {
                    t.Priority = ThreadPriority.AboveNormal;
                }
                else
                {
                    t.Priority = ThreadPriority.BelowNormal;
                }
                t.Start(i);
            }

            Thread.Sleep(500);
            Console.WriteLine("\tSe libera un turno");

            PC.Release(m);

            Thread.Sleep(o);

            Task banos = Task.Run(() => { ConstruirBanos(ba); });
            Task cuartos = Task.Run(() => { ConstruirHab(hb); });
            Task casas = Task.Run(() => { ConstruirCasas(c); });


            var ConstruirTask = new List<Task> { casas, banos, cuartos };
            while (ConstruirTask.Count > 0)
            {
                Task finishedTask = await Task.WhenAny(ConstruirTask);

                if (finishedTask == casas)
                {
                    Console.WriteLine("\tCasas construidas");
                }
                else if (finishedTask == banos)
                {
                    Console.WriteLine("\tBaños construidos");

                }
                else if (finishedTask == cuartos)
                {
                    Console.WriteLine("\tCuartos construidos");
                }
                ConstruirTask.Remove(finishedTask);
            }
            Console.WriteLine("\n La construccion a termido.");
            Console.ReadKey();


        }
    }
