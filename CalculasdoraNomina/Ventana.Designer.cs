﻿namespace CalculadoraNomina
{
    class ValoresConstantes
    {
        public const double PVac = 0.25 * vacaciones;
        public const double Importe = 0.004;
        public const double vejezP = 0.01125; 
        public const double inVidaP = 0.00625; 
        public const double prestacionesEP = 0.00375;
        public const int aguinaldo = 15;
        public const double prestacionesDP = 0.0025;
        public const int vacaciones = 6; 


    }


    partial class Ventana
    {


        //Componentes
        private System.ComponentModel.IContainer components = null;
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtDiario = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDias = new System.Windows.Forms.TextBox();
            this.txtImporte = new System.Windows.Forms.Label();
            this.lblImporte = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblISR = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblIMSS = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblSueldo = new System.Windows.Forms.Label();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblUMA = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblDeducciones = new System.Windows.Forms.Label();
            this.lblSub = new System.Windows.Forms.Label();
            this.lblSDI = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(37, 18);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Salario diario:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtDiario
            // 
            this.txtDiario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.txtDiario.Location = new System.Drawing.Point(173, 14);
            this.txtDiario.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtDiario.Name = "txtDiario";
            this.txtDiario.Size = new System.Drawing.Size(132, 26);
            this.txtDiario.TabIndex = 1;
            this.txtDiario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDiario_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(37, 58);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Días de nomina:";
            // 
            // txtDias
            // 
            this.txtDias.Location = new System.Drawing.Point(173, 55);
            this.txtDias.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtDias.Name = "txtDias";
            this.txtDias.Size = new System.Drawing.Size(132, 26);
            this.txtDias.TabIndex = 3;
            this.txtDias.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDias_KeyPress);
            // 
            // txtImporte
            // 
            this.txtImporte.AutoSize = true;
            this.txtImporte.Location = new System.Drawing.Point(43, 143);
            this.txtImporte.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.txtImporte.Name = "txtImporte";
            this.txtImporte.Size = new System.Drawing.Size(56, 20);
            this.txtImporte.TabIndex = 6;
            this.txtImporte.Text = "Sueldo:";
            // 
            // lblImporte
            // 
            this.lblImporte.AutoSize = true;
            this.lblImporte.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblImporte.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblImporte.Location = new System.Drawing.Point(221, 143);
            this.lblImporte.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblImporte.Name = "lblImporte";
            this.lblImporte.Size = new System.Drawing.Size(2, 22);
            this.lblImporte.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(43, 173);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(130, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = "Subsidio al empleo:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(43, 205);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 20);
            this.label5.TabIndex = 10;
            this.label5.Text = "ISR:";
            // 
            // lblISR
            // 
            this.lblISR.AutoSize = true;
            this.lblISR.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblISR.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblISR.Location = new System.Drawing.Point(221, 205);
            this.lblISR.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblISR.Name = "lblISR";
            this.lblISR.Size = new System.Drawing.Size(2, 22);
            this.lblISR.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(43, 238);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 20);
            this.label6.TabIndex = 12;
            this.label6.Text = "IMSS:";
            // 
            // lblIMSS
            // 
            this.lblIMSS.AutoSize = true;
            this.lblIMSS.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblIMSS.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblIMSS.Location = new System.Drawing.Point(221, 238);
            this.lblIMSS.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblIMSS.Name = "lblIMSS";
            this.lblIMSS.Size = new System.Drawing.Size(2, 22);
            this.lblIMSS.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(43, 361);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(118, 20);
            this.label7.TabIndex = 14;
            this.label7.Text = "Sueldo neto Total:";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // lblSueldo
            // 
            this.lblSueldo.AutoSize = true;
            this.lblSueldo.BackColor = System.Drawing.Color.Lime;
            this.lblSueldo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSueldo.Location = new System.Drawing.Point(223, 361);
            this.lblSueldo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSueldo.Name = "lblSueldo";
            this.lblSueldo.Size = new System.Drawing.Size(2, 22);
            this.lblSueldo.TabIndex = 15;
            // 
            // btnCalcular
            // 
            this.btnCalcular.Location = new System.Drawing.Point(152, 397);
            this.btnCalcular.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(110, 41);
            this.btnCalcular.TabIndex = 16;
            this.btnCalcular.Text = "Calcular ";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(43, 269);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(152, 20);
            this.label8.TabIndex = 17;
            this.label8.Text = "Salario base cotización:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(43, 305);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 20);
            this.label3.TabIndex = 19;
            this.label3.Text = "UMA";
            // 
            // lblUMA
            // 
            this.lblUMA.AutoSize = true;
            this.lblUMA.Location = new System.Drawing.Point(221, 305);
            this.lblUMA.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUMA.Name = "lblUMA";
            this.lblUMA.Size = new System.Drawing.Size(41, 20);
            this.lblUMA.TabIndex = 20;
            this.lblUMA.Text = "80.60";
            this.lblUMA.Click += new System.EventHandler(this.lblUMA_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(43, 337);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 20);
            this.label9.TabIndex = 21;
            this.label9.Text = "Deducciones:";
            // 
            // lblDeducciones
            // 
            this.lblDeducciones.AutoSize = true;
            this.lblDeducciones.BackColor = System.Drawing.Color.DodgerBlue;
            this.lblDeducciones.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDeducciones.Location = new System.Drawing.Point(223, 337);
            this.lblDeducciones.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDeducciones.Name = "lblDeducciones";
            this.lblDeducciones.Size = new System.Drawing.Size(2, 22);
            this.lblDeducciones.TabIndex = 22;
            // 
            // lblSub
            // 
            this.lblSub.AutoSize = true;
            this.lblSub.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblSub.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSub.Location = new System.Drawing.Point(221, 173);
            this.lblSub.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSub.Name = "lblSub";
            this.lblSub.Size = new System.Drawing.Size(2, 22);
            this.lblSub.TabIndex = 23;
            // 
            // lblSDI
            // 
            this.lblSDI.AutoSize = true;
            this.lblSDI.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblSDI.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSDI.Location = new System.Drawing.Point(221, 269);
            this.lblSDI.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSDI.Name = "lblSDI";
            this.lblSDI.Size = new System.Drawing.Size(2, 22);
            this.lblSDI.TabIndex = 24;
            // 
            // Ventana
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(399, 452);
            this.Controls.Add(this.lblSDI);
            this.Controls.Add(this.lblSub);
            this.Controls.Add(this.lblDeducciones);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lblUMA);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.lblSueldo);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lblIMSS);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblISR);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblImporte);
            this.Controls.Add(this.txtImporte);
            this.Controls.Add(this.txtDias);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtDiario);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Ventana";
            this.Text = "Calculadora de Nomina";
            this.Load += new System.EventHandler(this.Nomina_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        
        private System.Windows.Forms.TextBox txtDias;
        private System.Windows.Forms.Label txtImporte;
        private System.Windows.Forms.Label lblImporte;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDiario;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblISR;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblUMA;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblDeducciones;
        private System.Windows.Forms.Label lblSub;
        private System.Windows.Forms.Label lblSDI;
        private System.Windows.Forms.Label lblIMSS;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblSueldo;
        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label3;
       
    }
}

