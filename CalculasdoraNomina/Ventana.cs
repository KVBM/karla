﻿using System;
using System.Windows.Forms;

namespace CalculadoraNomina

{


    public partial class Ventana : Form
    {
        //Para inicializar la ventana
        public Ventana()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
            ///Crear las variables 
        {
            double dias = Convert.ToDouble(txtDias.Text);
            double SD = Convert.ToDouble(txtDiario.Text);
            double totalP = dias * SD;
            lblImporte.Text = totalP.ToString();
            
            double FI = 365+ ValoresConstantes.PVac + ValoresConstantes.aguinaldo + ValoresConstantes.vacaciones;
            FI = FI / 365;
            double SDI = FI * SD;
            double ISR;
            double ex;
            double PD;
            double PE;
            double inVida;
            double vejez;
            double Deducciones;
            double sub;
            double UMA;
            double dif;
            double importe2;
            double CFija;
            double ISRTotal;
            double sueldoTotal;
            double ImssT;
            

            lblSDI.Text = SDI.ToString();
            bool subsidio=false;
            UMA = Convert.ToDouble(lblUMA.Text);
          

            if (SD*30 <6000)
            {
                subsidio = true;
            }
            else
            {
                subsidio = false;
                sub = 0;
                lblSub.Text = sub.ToString();
            }
          

           

            if (SDI >= 0.01 || SDI <= 578.52)
            {
                ex = SDI - 0.01;
                ex *= 0.0192; // 1.92%
                CFija = 0;
                ex += CFija;
                ISR = CFija + ex;
                ISRTotal = SDI - ISR;
                lblISR.Text = ISRTotal.ToString();

                if ((UMA * 3) < SDI)
                {
                    dif = SDI - (UMA * 3);
                    importe2 = (dif * dias) * ValoresConstantes.Importe;//0.40%
                    PD = SDI * dias * ValoresConstantes.prestacionesDP;
                    PE = SDI * dias * ValoresConstantes.prestacionesEP;
                    inVida = SDI * dias * ValoresConstantes.inVidaP;
                    vejez = SDI * dias * ValoresConstantes.vejezP;
                    ImssT = dif + importe2 + PD + PE + inVida + vejez;
                    lblIMSS.Text = ImssT.ToString();
                    Deducciones = ISRTotal + ImssT;
                    lblDeducciones.Text = Deducciones.ToString();
                    sueldoTotal = totalP - Deducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio==true)
                    {
                        
                        if (totalP >= 0.01 || totalP <= 872.85)
                        {
                            sub = 200.85;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 872.86 && totalP < 1309.2)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1309.21 && totalP < 1713.6)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1713.61 && totalP < 1745.7)
                        {
                            sub = 193.8;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1745.71 && totalP < 2193.75)
                        {
                            sub = 188.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2193.76 && totalP < 2327.55)
                        {
                            sub = 174.75;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2327.56 && totalP < 2632.65)
                        {
                            sub = 160.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2632.66 && totalP < 3071.4)
                        {
                            sub = 145.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3071.41 && totalP < 3510.15)
                        {
                            sub = 1251.1;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3510.16 && totalP < 3642.6)
                        {
                            sub = 107.4;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3642.61)
                        {
                            sub = 0;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio==false)
                    {
                        sub = 0;
                        lblSub.Text = sub.ToString();
                        sueldoTotal += sub;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }



                }
                else if ((UMA * 3) > SDI)
                {
                    dif = 0;
                    importe2 = (dif * dias) * ValoresConstantes.Importe;
                    PD = SDI * dias * ValoresConstantes.prestacionesDP;
                    PE = SDI * dias * ValoresConstantes.prestacionesEP;
                    inVida = SDI * dias * ValoresConstantes.inVidaP;
                    vejez = SDI * dias * ValoresConstantes.vejezP;
                    ImssT = dif + importe2 + PD + PE + inVida + vejez;
                    lblIMSS.Text = ImssT.ToString();
                    Deducciones = ISRTotal + ImssT;
                    lblDeducciones.Text = Deducciones.ToString();
                    sueldoTotal = totalP - Deducciones;
                    lblSueldo.Text = sueldoTotal.ToString();
                }

            }
            else if (SDI >= 578.53 || SDI <= 4910.18)
            {
                ex = SDI - 578.53;
                ex *= 0.064;//6.40%
                CFija = 11.11;
                ex += CFija;
                ISR = CFija + ex;
                ISRTotal = SDI - ISR;
                lblISR.Text = ISRTotal.ToString();
                if ((UMA * 3) < SDI)
                {
                    dif = SDI - (UMA * 3);
                    importe2 = (dif * dias) * ValoresConstantes.Importe;
                    PD = SDI * dias * ValoresConstantes.prestacionesDP;
                    PE = SDI * dias * ValoresConstantes.prestacionesEP;
                    inVida = SDI * dias * ValoresConstantes.inVidaP;
                    vejez = SDI * dias * ValoresConstantes.vejezP;
                    ImssT = dif + importe2 + PD + PE + inVida + vejez;
                    lblIMSS.Text = ImssT.ToString();
                    Deducciones = ISRTotal + ImssT;
                    lblDeducciones.Text = Deducciones.ToString();
                    sueldoTotal = totalP - Deducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (totalP >= 0.01 || totalP <= 872.85)
                        {
                            sub = 200.85;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 872.86 || totalP <= 1309.2)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1309.21 || totalP <= 1713.6)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1713.61 || totalP <= 1745.7)
                        {
                            sub = 193.8;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1745.71 || totalP <= 2193.75)
                        {
                            sub = 188.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2193.76 || totalP <= 2327.55)
                        {
                            sub = 174.75;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2327.56 || totalP <= 2632.65)
                        {
                            sub = 160.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2632.66 || totalP <= 3071.4)
                        {
                            sub = 145.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3071.41 || totalP <= 3510.15)
                        {
                            sub = 1251.1;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3510.16 || totalP <= 3642.6)
                        {
                            sub = 107.4;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3642.61)
                        {
                            sub = 0;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio==false)
                    {
                        sub = 0;
                        lblSub.Text = sub.ToString();
                        sueldoTotal += sub;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
                else if ((UMA * 3) > SDI)
                {
                    dif = 0;
                    importe2 = (dif * dias) * ValoresConstantes.Importe;
                    PD = SDI * dias * ValoresConstantes.prestacionesDP;
                    PE = SDI * dias * ValoresConstantes.prestacionesEP;
                    inVida = SDI * dias * ValoresConstantes.inVidaP;
                    vejez = SDI * dias * ValoresConstantes.vejezP;
                    ImssT = dif + importe2 + PD + PE + inVida + vejez;
                    lblIMSS.Text = ImssT.ToString();
                    Deducciones = ISRTotal + ImssT;
                    lblDeducciones.Text = Deducciones.ToString();
                    sueldoTotal = totalP - Deducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (totalP >= 0.01 || totalP <= 872.85)
                        {
                            sub = 200.85;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 872.86 || totalP <= 1309.2)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1309.21 || totalP <= 1713.6)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1713.61 || totalP <= 1745.7)
                        {
                            sub = 193.8;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1745.71 || totalP <= 2193.75)
                        {
                            sub = 188.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2193.76 || totalP <= 2327.55)
                        {
                            sub = 174.75;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2327.56 || totalP <= 2632.65)
                        {
                            sub = 160.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2632.66 || totalP <= 3071.4)
                        {
                            sub = 145.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3071.41 || totalP <= 3510.15)
                        {
                            sub = 1251.1;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3510.16 || totalP <= 3642.6)
                        {
                            sub = 107.4;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3642.61)
                        {
                            sub = 0;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        sub = 0;
                        lblSub.Text = sub.ToString();
                        sueldoTotal += sub;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
            }
            else if (SDI >= 4910.19 || SDI <= 8629.20)
            {
                ex = SDI - 4910.19;
                ex *= 0.1088;//10.88%
                CFija = 288.33;
                ex += CFija;
                ISR = CFija + ex;
                ISRTotal = SDI - ISR;
                lblISR.Text = ISRTotal.ToString();
                if ((UMA * 3) < SDI)
                {
                    dif = SDI - (UMA * 3);
                    importe2 = (dif * dias) * ValoresConstantes.Importe;//0.0040%
                    PD = SDI * dias * ValoresConstantes.prestacionesDP;
                    PE = SDI * dias * ValoresConstantes.prestacionesEP;
                    inVida = SDI * dias * ValoresConstantes.inVidaP;
                    vejez = SDI * dias * ValoresConstantes.vejezP;
                    ImssT = dif + importe2 + PD + PE + inVida + vejez;
                    lblIMSS.Text = ImssT.ToString();
                    Deducciones = ISRTotal + ImssT;
                    lblDeducciones.Text = Deducciones.ToString();
                    sueldoTotal = totalP - Deducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (totalP >= 0.01 || totalP <= 872.85)
                        {
                            sub = 200.85;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 872.86 || totalP <= 1309.2)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1309.21 || totalP <= 1713.6)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1713.61 || totalP <= 1745.7)
                        {
                            sub = 193.8;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1745.71 || totalP <= 2193.75)
                        {
                            sub = 188.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2193.76 || totalP <= 2327.55)
                        {
                            sub = 174.75;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2327.56 || totalP <= 2632.65)
                        {
                            sub = 160.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2632.66 || totalP <= 3071.4)
                        {
                            sub = 145.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3071.41 || totalP <= 3510.15)
                        {
                            sub = 1251.1;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3510.16 || totalP <= 3642.6)
                        {
                            sub = 107.4;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3642.61)
                        {
                            sub = 0;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        sub = 0;
                        lblSub.Text = sub.ToString();
                        sueldoTotal += sub;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
                else if ((UMA * 3) > SDI)
                {
                    dif = 0;
                    importe2 = (dif * dias) * ValoresConstantes.Importe;
                    PD = SDI * dias * ValoresConstantes.prestacionesDP;
                    PE = SDI * dias * ValoresConstantes.prestacionesEP;
                    inVida = SDI * dias * ValoresConstantes.inVidaP;
                    vejez = SDI * dias * ValoresConstantes.vejezP;
                    ImssT = dif + importe2 + PD + PE + inVida + vejez;
                    lblIMSS.Text = ImssT.ToString();
                    Deducciones = ISRTotal + ImssT;
                    lblDeducciones.Text = Deducciones.ToString();
                    sueldoTotal = totalP - Deducciones;
                    lblSueldo.Text = sueldoTotal.ToString();
                    if (subsidio == true)
                    {

                        if (totalP >= 0.01 || totalP <= 872.85)
                        {
                            sub = 200.85;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 872.86 || totalP <= 1309.2)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1309.21 || totalP <= 1713.6)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1713.61 || totalP <= 1745.7)
                        {
                            sub = 193.8;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1745.71 || totalP <= 2193.75)
                        {
                            sub = 188.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2193.76 || totalP <= 2327.55)
                        {
                            sub = 174.75;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2327.56 || totalP <= 2632.65)
                        {
                            sub = 160.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2632.66 || totalP <= 3071.4)
                        {
                            sub = 145.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3071.41 || totalP <= 3510.15)
                        {
                            sub = 1251.1;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3510.16 || totalP <= 3642.6)
                        {
                            sub = 107.4;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3642.61)
                        {
                            sub = 0;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        sub = 0;
                        lblSub.Text = sub.ToString();
                        sueldoTotal += sub;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
            }

           
            else if (SDI >= 8629.21 || SDI <= 10031.07)
            {
                ex = SDI - 8629.21;
                ex *= 0.16;//%16;
                CFija = 692.96;
                ex += CFija;
                ISR = CFija + ex;
                ISRTotal = SDI - ISR;
                lblISR.Text = ISRTotal.ToString();
                if ((UMA * 3) < SDI)
                {
                    dif = SDI - (UMA * 3);
                    importe2 = (dif * dias) * ValoresConstantes.Importe;//0.0040%
                    PD = SDI * dias * ValoresConstantes.prestacionesDP;
                    PE = SDI * dias * ValoresConstantes.prestacionesEP;
                    inVida = SDI * dias * ValoresConstantes.inVidaP;
                    vejez = SDI * dias * ValoresConstantes.vejezP;
                    ImssT = dif + importe2 + PD + PE + inVida + vejez;
                    lblIMSS.Text = ImssT.ToString();
                    Deducciones = ISRTotal + ImssT;
                    lblDeducciones.Text = Deducciones.ToString();
                    sueldoTotal = totalP - Deducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (totalP >= 0.01 || totalP <= 872.85)
                        {
                            sub = 200.85;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 872.86 || totalP <= 1309.2)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1309.21 || totalP <= 1713.6)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1713.61 || totalP <= 1745.7)
                        {
                            sub = 193.8;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1745.71 || totalP <= 2193.75)
                        {
                            sub = 188.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2193.76 || totalP <= 2327.55)
                        {
                            sub = 174.75;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2327.56 || totalP <= 2632.65)
                        {
                            sub = 160.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2632.66 || totalP <= 3071.4)
                        {
                            sub = 145.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3071.41 || totalP <= 3510.15)
                        {
                            sub = 1251.1;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3510.16 || totalP <= 3642.6)
                        {
                            sub = 107.4;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3642.61)
                        {

                            sub = 0;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        sub = 0;
                        lblSub.Text = sub.ToString();
                        sueldoTotal += sub;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
                else if ((UMA * 3) > SDI)
                {
                    dif = 0;
                    importe2 = (dif * dias) * ValoresConstantes.Importe;
                    PD = SDI * dias * ValoresConstantes.prestacionesDP;
                    PE = SDI * dias * ValoresConstantes.prestacionesEP;
                    inVida = SDI * dias * ValoresConstantes.inVidaP;
                    vejez = SDI * dias * ValoresConstantes.vejezP;
                    ImssT = dif + importe2 + PD + PE + inVida + vejez;
                    lblIMSS.Text = ImssT.ToString();
                    Deducciones = ISRTotal + ImssT;
                    lblDeducciones.Text = Deducciones.ToString();
                    sueldoTotal = totalP - Deducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (totalP >= 0.01 || totalP <= 872.85)
                        {
                            sub = 200.85;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 872.86 || totalP <= 1309.2)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1309.21 || totalP <= 1713.6)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1713.61 || totalP <= 1745.7)
                        {
                            sub = 193.8;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1745.71 || totalP <= 2193.75)
                        {
                            sub = 188.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2193.76 || totalP <= 2327.55)
                        {
                            sub = 174.75;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2327.56 || totalP <= 2632.65)
                        {
                            sub = 160.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2632.66 || totalP <= 3071.4)
                        {
                            sub = 145.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3071.41 || totalP <= 3510.15)
                        {
                            sub = 1251.1;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3510.16 || totalP <= 3642.6)
                        {
                            sub = 107.4;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3642.61)
                        {

                            sub = 0;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        sub = 0;
                        lblSub.Text = sub.ToString();
                        sueldoTotal += sub;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }

                }
            }
            else if (SDI >= 10031.08 || SDI <= 12009.94)
            {
                ex = SDI - 10031.08;
                ex *= 0.1792;//17.92%
                CFija = 917.26;
                ex += CFija;
                ISR = CFija + ex;
                ISRTotal = SDI - ISR;
                lblISR.Text = ISRTotal.ToString();
                if ((UMA * 3) < SDI)
                {
                    dif = SDI - (UMA * 3);
                    importe2 = (dif * dias) * ValoresConstantes.Importe;
                    PD = SDI * dias * ValoresConstantes.prestacionesDP;
                    PE = SDI * dias * ValoresConstantes.prestacionesEP;
                    inVida = SDI * dias * ValoresConstantes.inVidaP;
                    vejez = SDI * dias * ValoresConstantes.vejezP;
                    ImssT = dif + importe2 + PD + PE + inVida + vejez;
                    lblIMSS.Text = ImssT.ToString();
                    Deducciones = ISRTotal + ImssT;
                    lblDeducciones.Text = Deducciones.ToString();
                    sueldoTotal = totalP - Deducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (totalP >= 0.01 || totalP <= 872.85)
                        {
                            sub = 200.85;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 872.86 || totalP <= 1309.2)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1309.21 || totalP <= 1713.6)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1713.61 || totalP <= 1745.7)
                        {
                            sub = 193.8;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1745.71 || totalP <= 2193.75)
                        {
                            sub = 188.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2193.76 || totalP <= 2327.55)
                        {
                            sub = 174.75;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2327.56 || totalP <= 2632.65)
                        {
                            sub = 160.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2632.66 || totalP <= 3071.4)
                        {
                            sub = 145.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3071.41 || totalP <= 3510.15)
                        {
                            sub = 1251.1;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3510.16 || totalP <= 3642.6)
                        {
                            sub = 107.4;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3642.61)
                        {
                            sub = 0;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        sub = 0;
                        lblSub.Text = sub.ToString();
                        sueldoTotal += sub;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
                else if ((UMA * 3) > SDI)
                {
                    dif = 0;
                    importe2 = (dif* dias) * ValoresConstantes.Importe;
                    PD = SDI * dias * ValoresConstantes.prestacionesDP;
                    PE = SDI * dias * ValoresConstantes.prestacionesEP;
                    inVida = SDI * dias * ValoresConstantes.inVidaP;
                    vejez = SDI * dias * ValoresConstantes.vejezP;
                    ImssT = dif + importe2 + PD + PE + inVida + vejez;
                    lblIMSS.Text = ImssT.ToString();
                    Deducciones = ISRTotal + ImssT;
                    lblDeducciones.Text = Deducciones.ToString();
                    sueldoTotal = totalP - Deducciones;
                    lblSueldo.Text = sueldoTotal.ToString();
                    if (subsidio == true)
                    {

                        if (totalP >= 0.01 || totalP <= 872.85)
                        {
                            sub = 200.85;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 872.86 || totalP <= 1309.2)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1309.21 || totalP <= 1713.6)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1713.61 || totalP <= 1745.7)
                        {
                            sub = 193.8;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1745.71 || totalP <= 2193.75)
                        {
                            sub = 188.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2193.76 || totalP <= 2327.55)
                        {
                            sub = 174.75;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2327.56 || totalP <= 2632.65)
                        {
                            sub = 160.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2632.66 || totalP <= 3071.4)
                        {
                            sub = 145.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3071.41 || totalP <= 3510.15)
                        {
                            sub = 1251.1;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3510.16 || totalP <= 3642.6)
                        {
                            sub = 107.4;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3642.61)
                        {
                            sub = 0;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        sub = 0;
                        lblSub.Text = sub.ToString();
                        sueldoTotal += sub;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }

            }
            else if (SDI >= 12009.95 || SDI <= 24222.31)
            {
                ex = SDI - 12009.95;
                ex *= 0.2126;//21.36%
                CFija = 1271.87;
                ex += CFija;
                ISR = CFija + ex;
                ISRTotal = SDI - ISR;
                lblISR.Text = ISRTotal.ToString();
                if ((UMA * 3) < SDI)
                {
                    dif = SDI - (UMA * 3);
                    importe2 = (dif* dias) * ValoresConstantes.Importe;
                    PD = SDI * dias * ValoresConstantes.prestacionesDP;
                    PE = SDI * dias * ValoresConstantes.prestacionesEP;
                    inVida = SDI * dias * ValoresConstantes.inVidaP;
                    vejez = SDI * dias * ValoresConstantes.vejezP;
                    ImssT = dif + importe2 + PD + PE + inVida + vejez;
                    lblIMSS.Text = ImssT.ToString();
                    Deducciones = ISRTotal + ImssT;
                    lblDeducciones.Text = Deducciones.ToString();
                    sueldoTotal = totalP - Deducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (totalP >= 0.01 || totalP <= 872.85)
                        {
                            sub = 200.85;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 872.86 || totalP <= 1309.2)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1309.21 || totalP <= 1713.6)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1713.61 || totalP <= 1745.7)
                        {
                            sub = 193.8;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1745.71 || totalP <= 2193.75)
                        {
                            sub = 188.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2193.76 || totalP <= 2327.55)
                        {
                            sub = 174.75;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2327.56 || totalP <= 2632.65)
                        {
                            sub = 160.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2632.66 || totalP <= 3071.4)
                        {
                            sub = 145.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3071.41 || totalP <= 3510.15)
                        {
                            sub = 1251.1;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3510.16 || totalP <= 3642.6)
                        {
                            sub = 107.4;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3642.61)
                        {
                            sub = 0;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        sub = 0;
                        lblSub.Text = sub.ToString();
                        sueldoTotal += sub;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
                else if ((UMA * 3) > SDI)
                {
                    dif = 0;
                    importe2 = (dif * dias) * ValoresConstantes.Importe;
                    PD = SDI * dias * ValoresConstantes.prestacionesDP;
                    PE = SDI * dias * ValoresConstantes.prestacionesEP;
                    inVida = SDI * dias * ValoresConstantes.inVidaP;
                    vejez = SDI * dias * ValoresConstantes.vejezP;
                    ImssT = dif + importe2 + PD + PE + inVida + vejez;
                    lblIMSS.Text = ImssT.ToString();
                    Deducciones = ISRTotal + ImssT;
                    lblDeducciones.Text = Deducciones.ToString();
                    sueldoTotal = totalP - Deducciones;
                    lblSueldo.Text = sueldoTotal.ToString();


                    //////////////////////////////////////////////////////////////////////
                    if (subsidio == true)
                    {

                        if (totalP >= 0.01 || totalP <= 872.85)
                        {
                            sub = 200.85;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 872.86 || totalP <= 1309.2)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1309.21 || totalP <= 1713.6)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1713.61 || totalP <= 1745.7)
                        {
                            sub = 193.8;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1745.71 || totalP <= 2193.75)
                        {
                            sub = 188.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2193.76 || totalP <= 2327.55)
                        {
                            sub = 174.75;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2327.56 || totalP <= 2632.65)
                        {
                            sub = 160.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2632.66 || totalP <= 3071.4)
                        {
                            sub = 145.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3071.41 || totalP <= 3510.15)
                        {
                            sub = 1251.1;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3510.16 || totalP <= 3642.6)
                        {
                            sub = 107.4;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3642.61)
                        {
                            sub = 0;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        sub = 0;
                        lblSub.Text = sub.ToString();
                        sueldoTotal += sub;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
            }
            else if (SDI >= 24222.32 || SDI <= 38177.69)
            {
                ex = SDI - 24222.32;
                ex *= 0.2352;//23.52%
                CFija = 3880.44;
                ex += CFija;
                ISR = CFija + ex;
                ISRTotal = SDI - ISR;
                lblISR.Text = ISRTotal.ToString();
                if ((UMA * 3) < SDI)
                {
                    dif = SDI - (UMA * 3);
                    importe2 = (dif * dias) * ValoresConstantes.Importe;
                    PD = SDI * dias * ValoresConstantes.prestacionesDP;
                    PE = SDI * dias * ValoresConstantes.prestacionesEP;
                    inVida = SDI * dias * ValoresConstantes.inVidaP;
                    vejez = SDI * dias * ValoresConstantes.vejezP;
                    ImssT = dif + importe2 + PD + PE + inVida + vejez;
                    lblIMSS.Text = ImssT.ToString();
                    Deducciones = ISRTotal + ImssT;
                    lblDeducciones.Text = Deducciones.ToString();
                    sueldoTotal = totalP - Deducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (totalP >= 0.01 || totalP <= 872.85)
                        {
                            sub = 200.85;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 872.86 || totalP <= 1309.2)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1309.21 || totalP <= 1713.6)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1713.61 || totalP <= 1745.7)
                        {
                            sub = 193.8;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1745.71 || totalP <= 2193.75)
                        {
                            sub = 188.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2193.76 || totalP <= 2327.55)
                        {
                            sub = 174.75;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2327.56 || totalP <= 2632.65)
                        {
                            sub = 160.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2632.66 || totalP <= 3071.4)
                        {
                            sub = 145.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3071.41 || totalP <= 3510.15)
                        {
                            sub = 1251.1;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3510.16 || totalP <= 3642.6)
                        {
                            sub = 107.4;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3642.61)
                        {
                            sub = 0;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        sub = 0;
                        lblSub.Text = sub.ToString();
                        sueldoTotal += sub;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
                else if ((UMA * 3) > SDI)
                {
                    dif = 0;
                    importe2 = (dif * dias) * ValoresConstantes.Importe;
                    PD = SDI * dias * ValoresConstantes.prestacionesDP;
                    PE = SDI * dias * ValoresConstantes.prestacionesEP;
                    inVida = SDI * dias * ValoresConstantes.inVidaP;
                    vejez = SDI * dias * ValoresConstantes.vejezP;
                    ImssT = dif + importe2 + PD + PE + inVida + vejez;
                    lblIMSS.Text = ImssT.ToString();
                    Deducciones = ISRTotal + ImssT;
                    lblDeducciones.Text = Deducciones.ToString();
                    sueldoTotal = totalP - Deducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    //////////////////////////S U B S I D I O //////////////////////////////7
                    if (subsidio == true)
                    {

                        if (totalP >= 0.01 || totalP <= 872.85)
                        {
                            sub = 200.85;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 872.86 || totalP <= 1309.2)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1309.21 || totalP <= 1713.6)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1713.61 || totalP <= 1745.7)
                        {
                            sub = 193.8;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1745.71 || totalP <= 2193.75)
                        {
                            sub = 188.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2193.76 || totalP <= 2327.55)
                        {
                            sub = 174.75;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2327.56 || totalP <= 2632.65)
                        {
                            sub = 160.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2632.66 || totalP <= 3071.4)
                        {
                            sub = 145.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3071.41 || totalP <= 3510.15)
                        {
                            sub = 1251.1;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3510.16 || totalP <= 3642.6)
                        {
                            sub = 107.4;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3642.61)
                        {
                            sub = 0;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        sub = 0;
                        lblSub.Text = sub.ToString();
                        sueldoTotal += sub;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
            }
            else if (SDI >= 38177.70 || SDI <= 72887.50)
            {
                ex = SDI - 38177.70;
                ex *= 0.3;//3%
                CFija = 7162.74;
                ex += CFija;
                ISR = CFija + ex;
                ISRTotal = SDI - ISR;
                lblISR.Text = ISRTotal.ToString();
                if ((UMA * 3) < SDI)
                {
                    dif = SDI - (UMA * 3);
                    importe2 = (dif * dias) * ValoresConstantes.Importe;
                    PD = SDI * dias * ValoresConstantes.prestacionesDP;
                    PE = SDI * dias * ValoresConstantes.prestacionesEP;
                    inVida = SDI * dias * ValoresConstantes.inVidaP;
                    vejez = SDI * dias * ValoresConstantes.vejezP;
                    ImssT = dif + importe2 + PD + PE + inVida + vejez;
                    lblIMSS.Text = ImssT.ToString();
                    Deducciones = ISRTotal + ImssT;
                    lblDeducciones.Text = Deducciones.ToString();
                    sueldoTotal = totalP - Deducciones;
                    lblSueldo.Text = sueldoTotal.ToString();



                    /////////////////////////////////////////////////////////////////////7
                    if (subsidio == true)
                    {

                        if (totalP >= 0.01 || totalP <= 872.85)
                        {
                            sub = 200.85;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 872.86 || totalP <= 1309.2)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1309.21 || totalP <= 1713.6)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1713.61 || totalP <= 1745.7)
                        {
                            sub = 193.8;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                        }
                        else if (totalP >= 1745.71 || totalP <= 2193.75)
                        {
                            sub = 188.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2193.76 || totalP <= 2327.55)
                        {
                            sub = 174.75;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2327.56 || totalP <= 2632.65)
                        {
                            sub = 160.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2632.66 || totalP <= 3071.4)
                        {
                            sub = 145.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3071.41 || totalP <= 3510.15)
                        {
                            sub = 1251.1;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3510.16 || totalP <= 3642.6)
                        {
                            sub = 107.4;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3642.61)
                        {
                            sub = 0;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }

                    else if (subsidio == false)
                    {
                        sub = 0;
                        lblSub.Text = sub.ToString();
                        sueldoTotal += sub;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
                else if ((UMA * 3) > SDI)
                {
                    dif = 0;
                    importe2 = (dif * dias) * ValoresConstantes.Importe;
                    PD = SDI * dias * ValoresConstantes.prestacionesDP;
                    PE = SDI * dias * ValoresConstantes.prestacionesEP;
                    inVida = SDI * dias * ValoresConstantes.inVidaP;
                    vejez = SDI * dias * ValoresConstantes.vejezP;
                    ImssT = dif + importe2 + PD + PE + inVida + vejez;
                    lblIMSS.Text = ImssT.ToString();
                    Deducciones = ISRTotal + ImssT;
                    lblDeducciones.Text = Deducciones.ToString();
                    sueldoTotal = totalP - Deducciones;
                    lblSueldo.Text = sueldoTotal.ToString();


                    if (subsidio == true)
                    {

                        if (totalP >= 0.01 || totalP <= 872.85)
                        {
                            sub = 200.85;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 872.86 || totalP <= 1309.2)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1309.21 || totalP <= 1713.6)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1713.61 || totalP <= 1745.7)
                        {
                            sub = 193.8;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                        }
                        else if (totalP >= 1745.71 || totalP <= 2193.75)
                        {
                            sub = 188.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2193.76 || totalP <= 2327.55)
                        {
                            sub = 174.75;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2327.56 || totalP <= 2632.65)
                        {
                            sub = 160.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2632.66 || totalP <= 3071.4)
                        {
                            sub = 145.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3071.41 || totalP <= 3510.15)
                        {
                            sub = 1251.1;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3510.16 || totalP <= 3642.6)
                        {
                            sub = 107.4;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3642.61)
                        {
                            sub = 0;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }

                    else if (subsidio == false)
                    {
                        sub = 0;
                        lblSub.Text = sub.ToString();
                        sueldoTotal += sub;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
            }
            else if (SDI >= 72887.51 || SDI <= 97183.33)
            {
                ex = SDI - 72887.51;
                ex *= 0.32;//32%
                CFija = 17575.69;
                ex += CFija;
                ISR = CFija + ex;
                ISRTotal = SDI - ISR;
                lblISR.Text = ISRTotal.ToString();
                if ((UMA * 3) < SDI)
                {
                    dif = SDI - (UMA * 3);
                    importe2 = (dif * dias) * ValoresConstantes.Importe;
                    PD = SDI * dias * ValoresConstantes.prestacionesDP;
                    PE = SDI * dias * ValoresConstantes.prestacionesEP;
                    inVida = SDI * dias * ValoresConstantes.inVidaP;
                    vejez = SDI * dias * ValoresConstantes.vejezP;
                    ImssT = dif + importe2 + PD + PE + inVida + vejez;
                    lblIMSS.Text = ImssT.ToString();
                    Deducciones = ISRTotal + ImssT;
                    lblDeducciones.Text = Deducciones.ToString();
                    sueldoTotal = totalP - Deducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (totalP >= 0.01 || totalP <= 872.85)
                        {
                            sub = 200.85;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 872.86 || totalP <= 1309.2)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1309.21 || totalP <= 1713.6)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1713.61 || totalP <= 1745.7)
                        {
                            sub = 193.8;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1745.71 || totalP <= 2193.75)
                        {
                            sub = 188.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2193.76 || totalP <= 2327.55)
                        {
                            sub = 174.75;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2327.56 || totalP <= 2632.65)
                        {
                            sub = 160.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2632.66 || totalP <= 3071.4)
                        {
                            sub = 145.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3071.41 || totalP <= 3510.15)
                        {
                            sub = 1251.1;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3510.16 || totalP <= 3642.6)
                        {
                            sub = 107.4;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3642.61)
                        {
                            sub = 0;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        sub = 0;
                        lblSub.Text = sub.ToString();
                        sueldoTotal += sub;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
                else if ((UMA * 3) > SDI)
                {
                    dif = 0;
                    importe2 = (dif * dias) * ValoresConstantes.Importe;
                    PD = SDI * dias * ValoresConstantes.prestacionesDP;
                    PE = SDI * dias * ValoresConstantes.prestacionesEP;
                    inVida = SDI * dias * ValoresConstantes.inVidaP;
                    vejez = SDI * dias * ValoresConstantes.vejezP;
                    ImssT = dif + importe2 + PD + PE + inVida + vejez;
                    lblIMSS.Text = ImssT.ToString();
                    Deducciones = ISRTotal + ImssT;
                    lblDeducciones.Text = Deducciones.ToString();
                    sueldoTotal = totalP - Deducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (totalP >= 0.01 || totalP <= 872.85)
                        {
                            sub = 200.85;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 872.86 || totalP <= 1309.2)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1309.21 || totalP <= 1713.6)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1713.61 || totalP <= 1745.7)
                        {
                            sub = 193.8;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1745.71 || totalP <= 2193.75)
                        {
                            sub = 188.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2193.76 || totalP <= 2327.55)
                        {
                            sub = 174.75;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2327.56 || totalP <= 2632.65)
                        {
                            sub = 160.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2632.66 || totalP <= 3071.4)
                        {
                            sub = 145.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3071.41 || totalP <= 3510.15)
                        {
                            sub = 1251.1;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3510.16 || totalP <= 3642.6)
                        {
                            sub = 107.4;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3642.61)
                        {
                            sub = 0;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        sub = 0;
                        lblSub.Text = sub.ToString();
                        sueldoTotal += sub;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
            }
            else if (SDI >= 97183.34 || SDI <= 291550)
            {
                ex = SDI - 97183.34;
                ex *= 0.34;//34%
                CFija = 25350.35;
                ex += CFija;
                ISR = CFija + ex;
                ISRTotal = SDI - ISR;
                lblISR.Text = ISRTotal.ToString();
                if ((UMA * 3) < SDI)
                {
                    dif = SDI - (UMA * 3);
                    importe2 = (dif * dias) * ValoresConstantes.Importe;
                    PD = SDI * dias * ValoresConstantes.prestacionesDP;
                    PE = SDI * dias * ValoresConstantes.prestacionesEP;
                    inVida = SDI * dias * ValoresConstantes.inVidaP;
                    vejez = SDI * dias * ValoresConstantes.vejezP;
                    ImssT = dif + importe2 + PD + PE + inVida + vejez;
                    lblIMSS.Text = ImssT.ToString();
                    Deducciones = ISRTotal + ImssT;
                    lblDeducciones.Text = Deducciones.ToString();
                    sueldoTotal = totalP - Deducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (totalP >= 0.01 || totalP <= 872.85)
                        {
                            sub = 200.85;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 872.86 || totalP <= 1309.2)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1309.21 || totalP <= 1713.6)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1713.61 || totalP <= 1745.7)
                        {
                            sub = 193.8;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1745.71 || totalP <= 2193.75)
                        {
                            sub = 188.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2193.76 || totalP <= 2327.55)
                        {
                            sub = 174.75;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2327.56 || totalP <= 2632.65)
                        {
                            sub = 160.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2632.66 || totalP <= 3071.4)
                        {
                            sub = 145.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3071.41 || totalP <= 3510.15)
                        {
                            sub = 1251.1;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3510.16 || totalP <= 3642.6)
                        {
                            sub = 107.4;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3642.61)
                        {
                            sub = 0;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        sub = 0;
                        lblSub.Text = sub.ToString();
                        sueldoTotal += sub;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
                else if ((UMA * 3) > SDI)
                {
                    dif = 0;
                    importe2 = (dif * dias) * ValoresConstantes.Importe;
                    PD = SDI * dias * ValoresConstantes.prestacionesDP;
                    PE = SDI * dias * ValoresConstantes.prestacionesEP;
                    inVida = SDI * dias * ValoresConstantes.inVidaP;
                    vejez = SDI * dias * ValoresConstantes.vejezP;
                    ImssT = dif + importe2 + PD + PE + inVida + vejez;
                    lblIMSS.Text = ImssT.ToString();
                    Deducciones = ISRTotal + ImssT;
                    lblDeducciones.Text = Deducciones.ToString();
                    sueldoTotal = totalP - Deducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (totalP >= 0.01 || totalP <= 872.85)
                        {
                            sub = 200.85;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 872.86 || totalP <= 1309.2)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1309.21 || totalP <= 1713.6)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1713.61 || totalP <= 1745.7)
                        {
                            sub = 193.8;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1745.71 || totalP <= 2193.75)
                        {
                            sub = 188.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2193.76 || totalP <= 2327.55)
                        {
                            sub = 174.75;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2327.56 || totalP <= 2632.65)
                        {
                            sub = 160.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2632.66 || totalP <= 3071.4)
                        {
                            sub = 145.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3071.41 || totalP <= 3510.15)
                        {
                            sub = 1251.1;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3510.16 || totalP <= 3642.6)
                        {
                            sub = 107.4;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3642.61)
                        {
                            sub = 0;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        sub = 0;
                        lblSub.Text = sub.ToString();
                        sueldoTotal += sub;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }


                }
            }
            else if (SDI >= 291550.01 || SDI <= 99999999.99)
            {
                ex = SDI - 291550.01;
                ex *= 0.35;//35%
                CFija = 91435.02;
                ex += CFija;
                ISR = CFija + ex;
                ISRTotal = SDI - ISR;
                lblISR.Text = ISRTotal.ToString();
                if ((UMA * 3) < SDI)
                {
                    dif = SDI - (UMA * 3);
                    importe2 = (dif * dias) * ValoresConstantes.Importe;//0.0040%
                    PD = SDI * dias * ValoresConstantes.prestacionesDP;
                    PE = SDI * dias * ValoresConstantes.prestacionesEP;
                    inVida = SDI * dias * ValoresConstantes.inVidaP;
                    vejez = SDI * dias * ValoresConstantes.vejezP;
                    ImssT = dif + importe2 + PD + PE + inVida + vejez;
                    lblIMSS.Text = ImssT.ToString();
                    Deducciones = ISRTotal + ImssT;
                    lblDeducciones.Text = Deducciones.ToString();
                    sueldoTotal = totalP - Deducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (totalP >= 0.01 || totalP <= 872.85)
                        {
                            sub = 200.85;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 872.86 || totalP <= 1309.2)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1309.21 || totalP <= 1713.6)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1713.61 || totalP <= 1745.7)
                        {
                            sub = 193.8;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1745.71 || totalP <= 2193.75)
                        {
                            sub = 188.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2193.76 || totalP <= 2327.55)
                        {
                            sub = 174.75;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2327.56 || totalP <= 2632.65)
                        {
                            sub = 160.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2632.66 || totalP <= 3071.4)
                        {
                            sub = 145.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3071.41 || totalP <= 3510.15)
                        {
                            sub = 1251.1;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3510.16 || totalP <= 3642.6)
                        {
                            sub = 107.4;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3642.61)
                        {
                            sub = 0;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        sub = 0;
                        lblSub.Text = sub.ToString();
                        sueldoTotal += sub;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
                else if ((UMA * 3) > SDI)
                {
                    dif = 0;
                    importe2 = (dif * dias) * ValoresConstantes.Importe;
                    PD = SDI * dias * ValoresConstantes.prestacionesDP;
                    PE = SDI * dias * ValoresConstantes.prestacionesEP;
                    inVida = SDI * dias * ValoresConstantes.inVidaP;
                    vejez = SDI * dias * ValoresConstantes.vejezP;
                    ImssT = dif + importe2 + PD + PE + inVida + vejez;
                    lblIMSS.Text = ImssT.ToString();
                    Deducciones = ISRTotal + ImssT;
                    lblDeducciones.Text = Deducciones.ToString();
                    sueldoTotal = totalP - Deducciones;
                    lblSueldo.Text = sueldoTotal.ToString();

                    if (subsidio == true)
                    {

                        if (totalP >= 0.01 || totalP <= 872.85)
                        {
                            sub = 200.85;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 872.86 || totalP <= 1309.2)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1309.21 || totalP <= 1713.6)
                        {
                            sub = 200.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1713.61 || totalP <= 1745.7)
                        {
                            sub = 193.8;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 1745.71 || totalP <= 2193.75)
                        {
                            sub = 188.7;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2193.76 || totalP <= 2327.55)
                        {
                            sub = 174.75;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2327.56 || totalP <= 2632.65)
                        {
                            sub = 160.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 2632.66 || totalP <= 3071.4)
                        {
                            sub = 145.35;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3071.41 || totalP <= 3510.15)
                        {
                            sub = 1251.1;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3510.16 || totalP <= 3642.6)
                        {
                            sub = 107.4;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                        else if (totalP >= 3642.61)
                        {
                            sub = 0;
                            lblSub.Text = sub.ToString();
                            sueldoTotal += sub;
                            lblSueldo.Text = sueldoTotal.ToString();
                        }
                    }
                    else if (subsidio == false)
                    {
                        sub = 0;
                        lblSub.Text = sub.ToString();
                        sueldoTotal += sub;
                        lblSueldo.Text = sueldoTotal.ToString();
                    }
                }
            }
        }

        private void txtDiario_KeyPress(object sender, KeyPressEventArgs v)
        {
            if (Char.IsDigit(v.KeyChar))
            {
                v.Handled = false;
            }
            else if (Char.IsSeparator(v.KeyChar))
            {
                v.Handled = false;
            }
            else if (Char.IsControl(v.KeyChar))
            {
                v.Handled = false;
            }
            else if (v.KeyChar.ToString().Equals("."))
            {
                v.Handled = false;
            }
            else
            {
                v.Handled = true;
                MessageBox.Show("Solo numeros o numeros con punto decimal");
            }
        }

        private void txtDias_KeyPress(object sender, KeyPressEventArgs v)
        {
            if (Char.IsDigit(v.KeyChar))
            {
                v.Handled = false;
            }
            else if (Char.IsSeparator(v.KeyChar))
            {
                v.Handled = false;
            }
            else if (Char.IsControl(v.KeyChar))
            {
                v.Handled = false;
            }
            else if (v.KeyChar.ToString().Equals("."))
            {
                v.Handled = false;
            }
            else
            {
                v.Handled = true;
                MessageBox.Show("Solo numeros o numeros con punto decimal");
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Nomina_Load(object sender, EventArgs e)
        {

        }

        private void lblUMA_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }
    }
}