﻿namespace Carrera
{
    partial class Ventana
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.p1 = new System.Windows.Forms.ProgressBar();
            this.p2 = new System.Windows.Forms.ProgressBar();
            this.p3 = new System.Windows.Forms.ProgressBar();
            this.Boton_Iniciar_Carrera = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // p1
            // 
            this.p1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.p1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.p1.Location = new System.Drawing.Point(93, 23);
            this.p1.Name = "p1";
            this.p1.Size = new System.Drawing.Size(373, 24);
            this.p1.TabIndex = 3;
            // 
            // p2
            // 
            this.p2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.p2.Location = new System.Drawing.Point(93, 65);
            this.p2.Name = "p2";
            this.p2.Size = new System.Drawing.Size(373, 26);
            this.p2.TabIndex = 4;
            // 
            // p3
            // 
            this.p3.ForeColor = System.Drawing.Color.Red;
            this.p3.Location = new System.Drawing.Point(93, 106);
            this.p3.Name = "p3";
            this.p3.Size = new System.Drawing.Size(373, 24);
            this.p3.TabIndex = 5;
            // 
            // Boton_Iniciar_Carrera
            // 
            this.Boton_Iniciar_Carrera.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.Boton_Iniciar_Carrera.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Boton_Iniciar_Carrera.Location = new System.Drawing.Point(392, 148);
            this.Boton_Iniciar_Carrera.Name = "Boton_Iniciar_Carrera";
            this.Boton_Iniciar_Carrera.Size = new System.Drawing.Size(63, 32);
            this.Boton_Iniciar_Carrera.TabIndex = 6;
            this.Boton_Iniciar_Carrera.Text = "Iniciar";
            this.Boton_Iniciar_Carrera.UseVisualStyleBackColor = false;
            this.Boton_Iniciar_Carrera.Click += new System.EventHandler(this.Boton_Iniciar_Carrera_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 19);
            this.label1.TabIndex = 7;
            this.label1.Text = "Auto1:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(23, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 19);
            this.label2.TabIndex = 8;
            this.label2.Text = "Auto 2:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(23, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 19);
            this.label3.TabIndex = 9;
            this.label3.Text = "Auto 3:";
            // 
            // CarreraCarros
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(490, 192);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Boton_Iniciar_Carrera);
            this.Controls.Add(this.p3);
            this.Controls.Add(this.p2);
            this.Controls.Add(this.p1);
            this.Name = "CarreraCarros";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CARRERRA DE AUTOS";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ProgressBar p1;
        private System.Windows.Forms.ProgressBar p2;
        private System.Windows.Forms.ProgressBar p3;
        private System.Windows.Forms.Button Boton_Iniciar_Carrera;
  
    }
}

