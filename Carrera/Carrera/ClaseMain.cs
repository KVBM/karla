﻿using System;
using System.Windows.Forms;

namespace Carrera
{
    static class ClaseMain

    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Ventana());
        }
    }
}
