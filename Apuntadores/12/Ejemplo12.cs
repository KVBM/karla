﻿using System;
namespace UnsafeMode12
{
    class Ejemplo12
    {
        static void Main(string[] args)
        {
            unsafe
            {
                int* a = stackalloc int[10];
                for (int i = 0; i < 10; ++i)
                    Console.WriteLine(a[i]);   // Print raw memory
                Console.ReadKey();
            }
        }
    }
}
