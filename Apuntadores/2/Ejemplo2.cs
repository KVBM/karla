﻿using System;
namespace Ejemplo2
{
    class Ejemplo2
    {
        unsafe static void Main()
        {
            fixed (char* value = "safe")
            {
                char* ptr = value;
                while (*ptr != '\0')
                {
                    Console.WriteLine(*ptr);
                    ++ptr;
                }
            }
            Console.ReadKey();
        }
    }
}
