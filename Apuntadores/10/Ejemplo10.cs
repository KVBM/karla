﻿using System;
namespace Ejemplo10
{
    class Ejemplo10
    {
        public int X;
        static void Main(string[] args)
        {
            Ejemplo10 test = new Ejemplo10();
            unsafe
            {
                fixed (int* p = &test.X) 
                {
                    *p = 9;
                }
                Console.WriteLine(test.X);
            }
            Console.ReadKey();
        }
        }
    }
