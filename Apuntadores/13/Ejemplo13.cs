﻿using System;
namespace Ejemplo13
{
   
    unsafe struct UnsafeUnicodeString
    {
        public short Length;
        public fixed byte Buffer[30];
    }
   unsafe class Ejemplo13
    {
        UnsafeUnicodeString uus;
        public Ejemplo13(string s)
        {
            uus.Length = (short)s.Length;
            fixed (byte* p = uus.Buffer)
                for (int i = 0; i < s.Length; i++)
                    p[i] = (byte)s[i];
        }
        static void Main(string[] args)
        {
            new Ejemplo13("Christian Troy");
            Console.ReadKey();
        }
    }
}
