﻿using System;
namespace Ejemplo3
{
    class Ejemplo3
    {
        static  unsafe void Main(string[] args)
        {

            int var = 20;
            int* p = &var;
            Console.WriteLine("Data is: {0} ", var);
            Console.WriteLine("Data is: {0} ", p->ToString());
            Console.WriteLine("Address is: {0} ", (int)p);
            Console.ReadKey();
        }
    }
}
