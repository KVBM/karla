﻿using System;
namespace Ejemplo11

{ 
    struct Test
    {
        public int X;
    }
    class Ejemplo11
    {
        unsafe static void Main(string[] args)
        {
            Test test = new Test(); 
            Test* p = &test;
            p->X = 9;
            Console.WriteLine(test.X);
            Console.ReadKey();
        }
    }
}
