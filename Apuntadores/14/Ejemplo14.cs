﻿using System;
namespace UnsafeMode14
{
    class Ejemplo14
    {
       unsafe static void Main(string[] args)
        {
            short[] a = { 1, 1, 2, 3, 5, 8, 13, 21, 34, 55 };
            fixed (short* p = a)
            {
                Zap(p, a.Length * sizeof(short));
            }
            foreach (short x in a)
                Console.WriteLine(x);  
            Console.ReadKey();
        }
        
        unsafe static void Zap(void* memory, int byteCount)
        {
            byte* b = (byte*)memory;
            for (int i = 0; i < byteCount; i++)
                *b++ = 0;
        }
    }
}
