package CuartoSemestre;
import java.util.Stack;//LIBRERIA PARA UTILIZAR PILAS

public class Exam {

	public static void main(String[] args) {
	    Stack pila1=new Stack();//CREAMOS PILA 
	    Stack pila2=new Stack();//CREAMOS PILA
	    Stack pila3=new Stack();//CREAMOS PILA 
        pila1.push(1);
        pila1.push(1);
        pila1.push(1);
        pila1.push(2);
        pila1.push(3);
        
        pila2.push(2);
        pila2.push(3);
        pila2.push(4);
        
        pila3.push(1);
        pila3.push(4);
        pila3.push(1);
        pila3.push(1);
        //imprimir las pilas
        System.out.print("Pila 1:\n");
	    for(int i = 0; i<pila1.size(); i++){                       
            System.out.println(pila1.get(i));               
       } 
        System.out.print("Pila 2:\n");
	    for(int i = 0; i<pila2.size(); i++){                       
            System.out.println(pila2.get(i));               
       } 
        System.out.print("Pila 3:\n");
	    for(int i = 0; i<pila3.size(); i++){                       
            System.out.println(pila3.get(i));               
	    }
	    
	    
	    System.out.print("\nNueva pila 1:\n");
	    for(int i = 0; i<1; i++){                       
            pila1.pop();               
            
	    }
	    for(int i = 0; i<pila1.size(); i++){                       
            System.out.println(pila1.get(i));               
	    }
	    System.out.print("\nNueva pila 2:\n");
	    for(int i = 0; i<1; i++){                       
            pila2.pop();               
            
	    }
	    for(int i = 0; i<pila2.size(); i++){                       
            System.out.println(pila2.get(i));               
	    }
	    System.out.print("\nNueva pila 3:\n");
	    for(int i = 0; i<2; i++){                       
            pila3.pop();               
            
	    }
	    for(int i = 0; i<pila3.size(); i++){                       
            System.out.println(pila3.get(i));               
	    }



	}

}
