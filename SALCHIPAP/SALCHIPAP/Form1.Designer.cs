﻿namespace SALCHIPAP
{
    partial class Ventana
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Ventana));
            this.panelRosa = new System.Windows.Forms.Panel();
            this.buttonMinimizar = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.botonX = new System.Windows.Forms.Button();
            this.botonPaquetes = new System.Windows.Forms.Button();
            this.botonSalir = new System.Windows.Forms.Button();
            this.botonPedidos = new System.Windows.Forms.Button();
            this.botonCuenta = new System.Windows.Forms.Button();
            this.LOGO = new System.Windows.Forms.PictureBox();
            this.panelContenedor = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.LOGO)).BeginInit();
            this.SuspendLayout();
            // 
            // panelRosa
            // 
            this.panelRosa.BackColor = System.Drawing.Color.DeepPink;
            this.panelRosa.Location = new System.Drawing.Point(0, -1);
            this.panelRosa.Name = "panelRosa";
            this.panelRosa.Size = new System.Drawing.Size(1170, 12);
            this.panelRosa.TabIndex = 8;
            this.panelRosa.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panelRosa_MouseMove);
            // 
            // buttonMinimizar
            // 
            this.buttonMinimizar.BackColor = System.Drawing.Color.Transparent;
            this.buttonMinimizar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonMinimizar.BackgroundImage")));
            this.buttonMinimizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonMinimizar.FlatAppearance.BorderSize = 0;
            this.buttonMinimizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMinimizar.ForeColor = System.Drawing.Color.PaleGreen;
            this.buttonMinimizar.Location = new System.Drawing.Point(1042, 30);
            this.buttonMinimizar.Name = "buttonMinimizar";
            this.buttonMinimizar.Size = new System.Drawing.Size(30, 23);
            this.buttonMinimizar.TabIndex = 7;
            this.buttonMinimizar.UseVisualStyleBackColor = false;
            this.buttonMinimizar.Click += new System.EventHandler(this.buttonMinimizar_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.PaleGreen;
            this.button1.Location = new System.Drawing.Point(563, 373);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(0, 0);
            this.button1.TabIndex = 6;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // botonX
            // 
            this.botonX.BackColor = System.Drawing.Color.Transparent;
            this.botonX.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("botonX.BackgroundImage")));
            this.botonX.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.botonX.FlatAppearance.BorderSize = 0;
            this.botonX.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.botonX.ForeColor = System.Drawing.Color.PaleGreen;
            this.botonX.Location = new System.Drawing.Point(1090, 30);
            this.botonX.Name = "botonX";
            this.botonX.Size = new System.Drawing.Size(30, 23);
            this.botonX.TabIndex = 5;
            this.botonX.UseVisualStyleBackColor = false;
            this.botonX.Click += new System.EventHandler(this.botonX_Click);
            // 
            // botonPaquetes
            // 
            this.botonPaquetes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.botonPaquetes.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("botonPaquetes.BackgroundImage")));
            this.botonPaquetes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.botonPaquetes.FlatAppearance.BorderSize = 0;
            this.botonPaquetes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.botonPaquetes.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.botonPaquetes.Location = new System.Drawing.Point(533, 165);
            this.botonPaquetes.Margin = new System.Windows.Forms.Padding(0);
            this.botonPaquetes.Name = "botonPaquetes";
            this.botonPaquetes.Size = new System.Drawing.Size(174, 54);
            this.botonPaquetes.TabIndex = 4;
            this.botonPaquetes.UseVisualStyleBackColor = true;
            this.botonPaquetes.Click += new System.EventHandler(this.botonPaquetes_Click);
            // 
            // botonSalir
            // 
            this.botonSalir.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.botonSalir.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("botonSalir.BackgroundImage")));
            this.botonSalir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.botonSalir.FlatAppearance.BorderSize = 0;
            this.botonSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.botonSalir.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.botonSalir.Location = new System.Drawing.Point(953, 165);
            this.botonSalir.Margin = new System.Windows.Forms.Padding(0);
            this.botonSalir.Name = "botonSalir";
            this.botonSalir.Size = new System.Drawing.Size(174, 54);
            this.botonSalir.TabIndex = 3;
            this.botonSalir.UseVisualStyleBackColor = true;
            this.botonSalir.Click += new System.EventHandler(this.botonSalir_Click);
            // 
            // botonPedidos
            // 
            this.botonPedidos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.botonPedidos.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("botonPedidos.BackgroundImage")));
            this.botonPedidos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.botonPedidos.FlatAppearance.BorderSize = 0;
            this.botonPedidos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.botonPedidos.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.botonPedidos.Location = new System.Drawing.Point(314, 165);
            this.botonPedidos.Margin = new System.Windows.Forms.Padding(0);
            this.botonPedidos.Name = "botonPedidos";
            this.botonPedidos.Size = new System.Drawing.Size(174, 54);
            this.botonPedidos.TabIndex = 2;
            this.botonPedidos.UseVisualStyleBackColor = true;
            this.botonPedidos.Click += new System.EventHandler(this.botonPedidos_Click);
            // 
            // botonCuenta
            // 
            this.botonCuenta.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.botonCuenta.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("botonCuenta.BackgroundImage")));
            this.botonCuenta.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.botonCuenta.FlatAppearance.BorderSize = 0;
            this.botonCuenta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.botonCuenta.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.botonCuenta.Location = new System.Drawing.Point(747, 165);
            this.botonCuenta.Margin = new System.Windows.Forms.Padding(0);
            this.botonCuenta.Name = "botonCuenta";
            this.botonCuenta.Size = new System.Drawing.Size(174, 54);
            this.botonCuenta.TabIndex = 1;
            this.botonCuenta.UseVisualStyleBackColor = true;
            this.botonCuenta.Click += new System.EventHandler(this.botonCuenta_Click);
            // 
            // LOGO
            // 
            this.LOGO.Image = global::SALCHIPAP.Properties.Resources.logo_imagen;
            this.LOGO.Location = new System.Drawing.Point(8, 17);
            this.LOGO.Name = "LOGO";
            this.LOGO.Size = new System.Drawing.Size(1152, 123);
            this.LOGO.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.LOGO.TabIndex = 0;
            this.LOGO.TabStop = false;
            this.LOGO.MouseMove += new System.Windows.Forms.MouseEventHandler(this.LOGO_MouseMove);
            // 
            // panelContenedor
            // 
            this.panelContenedor.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panelContenedor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelContenedor.Location = new System.Drawing.Point(30, 255);
            this.panelContenedor.Name = "panelContenedor";
            this.panelContenedor.Size = new System.Drawing.Size(1111, 480);
            this.panelContenedor.TabIndex = 9;
            // 
            // Ventana
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(1169, 747);
            this.Controls.Add(this.panelContenedor);
            this.Controls.Add(this.panelRosa);
            this.Controls.Add(this.buttonMinimizar);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.botonX);
            this.Controls.Add(this.botonPaquetes);
            this.Controls.Add(this.botonSalir);
            this.Controls.Add(this.botonPedidos);
            this.Controls.Add(this.botonCuenta);
            this.Controls.Add(this.LOGO);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Ventana";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this.LOGO)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox LOGO;
        private System.Windows.Forms.Button botonCuenta;
        private System.Windows.Forms.Button botonPedidos;
        private System.Windows.Forms.Button botonSalir;
        private System.Windows.Forms.Button botonPaquetes;
        private System.Windows.Forms.Button botonX;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button buttonMinimizar;
        private System.Windows.Forms.Panel panelRosa;
        private System.Windows.Forms.Panel panelContenedor;
    }
}

