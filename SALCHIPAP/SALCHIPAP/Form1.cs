﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SALCHIPAP
{
    public partial class Ventana : Form
    {
        public Ventana()
        {
            InitializeComponent();
        }

        /////////////////MOVER LA VENTANA ////////////////////////////////////////////////////
        //VARIABLES PARA PANEL MOVE
        int posY = 0;
        int posX = 0;

        private void panelRosa_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)//MOVIMIENTO DE LA VENTANA
            {
                posX = e.X;
                posY = e.Y;
            }
            else
            {
                Left = Left + (e.X - posX);
                Top = Top + (e.Y - posY);
            }

        }

        private void LOGO_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)//MOVIMIENTO DE LA VENTANA
            {
                posX = e.X;
                posY = e.Y;
            }
            else
            {
                Left = Left + (e.X - posX);
                Top = Top + (e.Y - posY);
            }

        
    }
        /////////////////////////////////CLICK BOTONES////////////////////////////////////
        //BOTON CERRAR VENTANA
        private void botonX_Click(object sender, EventArgs e) //BOTON X
        {
            MessageBoxButtons botonesM = MessageBoxButtons.YesNo; //Creamos variable para los botoenes 
            DialogResult dr = MessageBox.Show("¿Seguro que deseas salir?", " ", botonesM, MessageBoxIcon.Exclamation);
            if (dr == DialogResult.Yes)
            {
                Application.Exit();///PARA SALIR DEL PROGRAMA 
            }

        }

        //BOTON MINIMIZAR VENTANA 
        private void buttonMinimizar_Click(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Normal)
            {
                WindowState = FormWindowState.Minimized; //PARA MINIMIZAR VENTANA
            }
        }


        //BOTON CUENTA

        private void botonCuenta_Click(object sender, EventArgs e)
        {
            //panelContenedor.Visible = true;

        }

        //BOTON SALIR 
        private void botonSalir_Click(object sender, EventArgs e)
        {
            //panelContenedor.Visible = false;
        }

        //BOTON PAQUETES
        private void botonPaquetes_Click(object sender, EventArgs e)
        {
            //panelContenedor.Visible = false;
        }

        //BOTON PEDIDOS
        private void botonPedidos_Click(object sender, EventArgs e)
        {
           // panelContenedor.Visible = false;
        }
    }
}
