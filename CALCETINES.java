package PRIMER_PARCIAL;

import java.util.Stack;//LIBRERIA PARA UTILIZAR PILAS
import java.util.Collections;//LIBRERIA PARA ORDENAR CON EL METODO Collections
import java.util.Scanner;//LIBRERIA PARA CAPTURAR POR TECLADO

public class CALCETINES {
	Scanner entrada=new Scanner(System.in);//VARIABLE PARA CAPTURAR POR TECLADO 
	private int n;//Numero de calcetines
	private int colores[];//Arreglo para guardar los colores de los calcetines
	private int x;//Conteo
	private int c1=0;//Para contar el color1
	private int c2=0;//Para contar el color2
	private int c3=0;//Para contar el color3
	private int c4=0;//Para contar el color4
	private int p1;//Para contar los pares del color 1
	private int p2;//Para contar los pares del color 2
	private int p3;//Para contar los pares del color 3
	private int p4;//Para contar los pares del color 4
	private int total;//Para calcular el total de pares
	
	
	void inicio() {
	System.out.print("Digite el numero de calcetines:  ");
	n=entrada.nextInt();
	//Declaramos la cantidad de colores
	colores=new int[n];//DECLARAMOS LA CANTIDAD DEl ARREGLO PARA GUARDAR EL COLOR DE LOS CALETINES
	System.out.print("Colores:\n   (Seleccione un color)\n________________________________\n");
	System.out.print("Rojo:  1       Gris:   2\n");
	System.out.print("Negro: 3       Blanco: 4\n");
	//Hacemos for para guardarlos colores de n calcetines
	for(int a=0;a<n;a++){
		x++;
		System.out.print("________________________________\nEliga color "+x+"�:  ");
		colores[a]=entrada.nextInt();
		//Conteo
		if(colores[a]==1) {//Si el color es 1 lo vamos contando
			c1++; 
		}
		if(colores[a]==2) {//Si el color es 2 lo vamos contando
			c2++;
		}
		if(colores[a]==3) {//Si el color es 3 lo vamos contando
			c3++;
		}
		if(colores[a]==4) {//Si el color es 4 lo vamos contando
			c4++;
		}
	}//for	
	}//void Inicio
	
	
	void CalcularPares() {
		//Color 1
		if(c1>=2&&(c1%2)==0) { //Si hay mas de dos calcetines del color 1 y si la cantidad es par
			p1=c1/2; //Calculamos los pares del color 1
		}
		if(c1>1&&(c1%2)!=0) {//Si hay mas de un calcetin del color 1 y si la cantidad no es par
			p1=(c1-1)/2; //Calculamos los pares del color 1
		}
		//Color2
		if(c2>=2&&(c2%2)==0) {//Si hay mas de dos calcetines del color 2 y si la cantidad es par
			p2=c2/2; //Calculamos los pares del color 2
		}
		if(c2>1&&(c2%2)!=0) {//Si hay mas de un calcetin del color 2 y si la cantidad no es par
			p2=(c2-1)/2; //Calculamos los pares del color 2
		}
		//Color3
		if(c3>=2&&(c3%2)==0) { //Si hay mas de dos calcetines del color 3 y si la cantidad es par
			p3=c3/2;
		}
		if(c3>1&&(c3%2)!=0) {//Si hay mas de un calcetin del color 3 y si la cantidad no es par
			p3=(c3-1)/2;
		}
		//Color4
		if(c4>=2&&(c4%2)==0) {//Si hay mas de dos calcetines del color 4 y si la cantidad es par
			p4=c4/2;
		}
		if(c4>1&&(c4%2)!=0) {//Si hay mas de un calcetin del color 4 y si la cantidad no es par
			p4=(c4-1)/2;
		}
		total=p1+p2+p3+p4;
		System.out.print("\n    Color:      Cantidad:       Pares:    ");
		System.out.print("\n    Rojo          "+c1+"               "+p1);
		System.out.print("\n    Gris          "+c2+"               "+p2);
		System.out.print("\n    Negro         "+c3+"               "+p3);
		System.out.print("\n    Blanco        "+c4+"               "+p4);
	    System.out.print("\n    Total de pares: "+total);
	}
	public static void main(String[] args) {
		CALCETINES in=new CALCETINES();
		in.inicio();
		in.CalcularPares();
	}
}
