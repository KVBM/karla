﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora
{
    public partial class Calculadora : Form
    {
        public Calculadora()
        {
            InitializeComponent();
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "0";
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "1";
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "2";
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "3";
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "4";
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "5";
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "6";
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "7";
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "8";
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "9";
        }

        private void buttonP_Click(object sender, EventArgs e)
        {
            txtResultado.Text += ".";
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            string texto = txtResultado.Text.Remove(txtResultado.Text.Length-1,1);
            txtResultado.Text = texto;
        }

        private void btnD_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "/";
        }

        private void btnM_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "*";
        }

        private void btnR_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "-";
        }

        private void btnS_Click(object sender, EventArgs e)
        {
            txtResultado.Text += "+";
        }
    }
}
