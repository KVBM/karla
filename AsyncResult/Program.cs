﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace EjemploAsynchronous
{

    class TaskBasedAsynchronousProgramming
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"Main Thread :\n {Thread.CurrentThread.ManagedThreadId} Statred");
            Task task1 = new Task(Impresiones);
            task1.Start();
            Console.WriteLine($"Main Thread :\n {Thread.CurrentThread.ManagedThreadId} Completed");

            Task task2 = Task.Factory.StartNew(Nomina);
            Task task3 = Task.Run(() => { Pagos(); });
            Console.WriteLine("\n\n_____________________________________________________\n\n");

            Console.ReadKey();
        }

        static void PrintCounter()
        {
            Console.WriteLine($"Child Thread : {Thread.CurrentThread.ManagedThreadId} Started");
            for (int count = 1; count <= 5; count++)
            {
                Console.WriteLine($"count value: {count}");
            }
            Console.WriteLine($"Child Thread : {Thread.CurrentThread.ManagedThreadId} Completed");
        }
        static void Nomina()
        {
            Console.WriteLine("\n\n_____________________________________________________\n\n");

            Console.WriteLine($"Child Thread : {Thread.CurrentThread.ManagedThreadId} Started");
            Console.WriteLine("Comienzo de nomina");
            for (int i = 0; i <= 5; i++)
            {
                Console.WriteLine("Desarrollando nomina {0}", i+1);
            }
            Console.WriteLine("Termino de nomina");
            Console.WriteLine($"Child Thread : {Thread.CurrentThread.ManagedThreadId} Completed");
        }
        static void Pagos()
        {
            Console.WriteLine("Cominezo de los  Pagos ");
            for (int i = 0; i <= 5; i++)
            {
                Console.WriteLine("Desarrollando el pago {0}", i+1);
            }
            Console.WriteLine("Termino de los Pagos ");
        }

        static void Impresiones()
        {

            Console.WriteLine($"Child Thread : {Thread.CurrentThread.ManagedThreadId} Completed");
            Console.WriteLine("Comienzo de Impresiones");
            for (int i = 0; i <= 5; i++)
            {
                Console.WriteLine("Desarrollando impresiones {0}", i+1);
            }
            Console.WriteLine("Termino de impresiones");
            Console.WriteLine($"Child Thread : {Thread.CurrentThread.ManagedThreadId} Completed");
        }
    }
}

