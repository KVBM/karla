﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delegados
{
    //definimos el delegado con las caracteristicas que nos interesa
    public delegate void MiDelegado(string n);
    class Program
    {
        static void Main(string [] args)
        {
            //creamos un objeto del delegado y lo referimos a un metodo
            MiDelegado delegado = new MiDelegado(Cradio.MetodoRadio);

            //Ahora por medio del delegado hacemos uso del metodo
            delegado("Hola a todos");

            //Hacemos que apunte a otro metodo
            delegado = new MiDelegado(Cpastel.MostrarPastel);

            //Ahora invocamos el otro metodo
            delegado("Feliz Cumpleaños");

        }



    }



}