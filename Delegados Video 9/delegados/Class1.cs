﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delegados
{
    class Cradio
    {
        //este metodo actuara como delegado
        public static void MetodoRadio(string pMensaje)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Estamos en la clase llamada Radio ");
            Console.WriteLine("Este es tu mensaje {0}", pMensaje);



            //Console.WriteLine("Hello World!");
        }
    }
}
