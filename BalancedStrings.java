package PRIMER_PARCIAL;
import java.util.Scanner;//LIBRERIA PARA CAPTURAR POR TECLADO
import java.util.Stack;




public class BalancedStrings {
static char[] abrir = {'(', '[', '{'}; 
static char[] cerrar = {')', ']', '}'}; 

static boolean inicio (char c) {
	for(int i=0;i<abrir.length; i++) 
		if(abrir[i]==c)
			return true;
		return false;
	
}
static char getC(char c) {
	for(int i=0;i<abrir.length;i++)
		if(abrir[i]==c)
			return cerrar[i];
	for(int i=0;i<cerrar.length;i++) 
		if(cerrar[i]==c)
			return abrir[i];
		return ' ';
	
}
	

	
	public static void main(String[] args) {
		Scanner entrada=new Scanner(System.in);
		System.out.print("\n\t\t___________________________________________");
		System.out.print("\n\t\tEscriba una cadena de caracteres: ");
		String cadena=entrada.next();
		char[] cadena1=cadena.toCharArray();
		Stack<Character> s=new Stack<>();
		boolean correcto=true;
		for(int i=0;i<cadena1.length;i++) {
			if(inicio(cadena1[i]))
				s.push(cadena1[i]);
			else {
				if(s.isEmpty()) {
					correcto=false;
					break;
				}else if(s.peek() !=getC(cadena1[i])) {
					correcto=false;
					break;
					
				}else
					s.pop();
			}
		}
		if(!s.empty())
			correcto=false;
		if(correcto)
			
			System.out.println("\t\t___________________________________________\n\t\t   "+cadena+"  Esta balanceada");
		else
			System.out.println("\t\t___________________________________________\n\t\t   "+cadena+"  No esta balanceada");

	}

}
