﻿using System;
using System.Windows.Forms;

namespace ClienteWS
{
    public partial class FormPruebaWCF : Form
    {
        public FormPruebaWCF()
        {
            InitializeComponent();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            var identificacion = txtIdentificacion.Text;
            using (WSPersonas.WSPersonasClient client = new WSPersonas.WSPersonasClient())
            {
                var persona = client.ObtenerPersona(identificacion);
                var nombre = persona.Nombre;
            }

        }
    }
}
